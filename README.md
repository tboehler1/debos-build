# debos-build

This repository contains the recipes required to build an image for Kunbus
GmbH's Revolution Pi line.

## Building

Install the dependencies:

- [debos](https://packages.debian.org/stable/debos)

Building the "lite" Revolution Pi image:

```sh
make lite
```

### Flavours

There are multiple flavours of images that can be built. Each flavour builds
upon the previous (i.e. the `lite` image is an extended `basic` image, which in
turn is an extended `minimal` image).

- `minimal`: Absolute basic image to run a Revolution Pi. Doesn't include
  any RevPi-specific software that isn't absolutely needed to boot and run the
  system.
- `basic`: Includes basic software for a Revolution Pi system like piControl and
  PiCtory.
- `lite`: Comfortably usable image with few installed packages
- `default`: Provides a GUI as well as other useful software

### Options

The Makefile supports the following variables:

- `ARCH`: The architecture to build for. Default: `arm64`
- `SUITE`: The Debian release to build. Default: `bookworm`
- `IMAGE_SIZE`: The size of the output image. Default: `1.5GiB`
- `LOCAL_DEBS`: Whether to install Debian packages from the local `debs-to-install`
  directory. Default: `false`.
- `DO_IMAGE`: Produce an image as the output. Default: `true`
- `DO_TAR`: Produce a tarball as the output. Default: `false`
- `DO_COMPRESS`: Compress the output image. Default: `true`
- `DATE`: The value to use as the date field. Default: Output of `date
  +%Y-%m-%d`
- `VERSION`: The version the image is given. This is only used when a snapshot
  is built. Default: Output of `git describe ...`
- `DEBOS_FLAGS`: Additional flags to pass to debos. Overrides any flags used in
  the Makefile. Default: empty
- `RELEASE`: Build a release image. Default: `false`

For example, to create an `arm64` release image for `bookworm` as well as a
tarball for the `default` image flavour:

```sh
make ARCH=arm64 RELEASE=1 SUITE=bullseye DO_TAR=true
```

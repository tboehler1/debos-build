DEBOS ?= debos

VERSION ?= $(shell git describe --long --abbrev=12 --tags --dirty --always 2>/dev/null)
DATE ?= $(shell date +%Y-%m-%d)

ARCH ?= arm64
SUITE ?= bookworm
LOCAL_DEBS ?= false
DO_IMAGE ?= true
DO_TAR ?= false
DO_COMPRESS ?= true
RELEASE ?= false

ifneq ($(RELEASE),false)
	_DATE = $(DATE)
else
	_DATE = $(VERSION)_$(DATE)
endif

_DEBOS_FLAGS ?= \
	-tarch:"$(ARCH)" \
	-tsuite:"$(SUITE)" \
	-tlocal_debs:"$(LOCAL_DEBS)" \
	-tdo_image:"$(DO_IMAGE)" \
	-tdo_tar:"$(DO_TAR)" \
	-tdo_compress:"$(DO_COMPRESS)"

# there exists a bug in debos where image sizes in base 1024 (i.e. mebi- or
# gibibytes) that are defined with floating point numbers (i.e. 1.5) are
# interpreted as base 1000, thus setting a wrong image size. By default, the
# preferred image size by the template should be used and not overridden, but a
# different size can still be given via the IMAGE_SIZE make variable.
ifdef IMAGE_SIZE
	_DEBOS_FLAGS += -timage_size:"$(IMAGE_SIZE)"
endif

__DEBOS_FLAGS = $(_DEBOS_FLAGS) $(DEBOS_FLAGS)

default: default

# main build recipe
%: revpi-template.yaml templates/%.yaml
	$(DEBOS) \
		-timage_type:"$@" \
		-timage:"$(_DATE)-revpi-$(SUITE)-$(ARCH)-$@.img" \
		$(__DEBOS_FLAGS) \
		$<

.PHONY: clean
clean:
	rm -rf *.img *.img.xz *.img.md5sum.txt *.tar.gz .debos-*
